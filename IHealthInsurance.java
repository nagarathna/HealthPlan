/**
 * 
 */
package com.healthInsurance.service;

import com.healthInsurance.bean.HealthInsuranceBean;

public interface IHealthInsurance {

	String checkPremium(HealthInsuranceBean healthInsuranceBean);

}
